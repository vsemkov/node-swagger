import express, { Application } from 'express'
import bodyParser from 'body-parser'
import swaggerUi from 'swagger-ui-express'
import * as swaggerDocument from './swagger/swagger.json'
import IController from './interfaces/controller.interface'
import mongoose from 'mongoose'
import path from 'path'

class App {
  public app: Application
  private port: number
  private dbUri = 'mongodb://test:mongo8763ed841d022ea@127.0.0.1:27017/test'

  constructor(controllers:IController[], port:number) {
    this.port = port
    this.app = express()

    this.setConfig()
    this.initializeControllers(controllers);
  }

  private setConfig() {
    this.app.use(bodyParser.json({ limit: '50mb' }));
    this.app.use(express.static('static'));
    this.app.use(bodyParser.urlencoded({ limit: '50mb', extended: true}));
    this.app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

    this.app.get('/', (req, res) => {
      res.sendFile(path.join(__dirname + '/index.html'));
    });
  }

  private initializeControllers(controllers: IController[]) {
    controllers.forEach((controller) => {
      this.app.use('/', controller.getRouter());
    });
  }

  public start = () => {
    return new Promise((resolve, reject) => {
        this.app.listen(
        this.port,
        () => {
            mongoose.connect(this.dbUri, {
              useNewUrlParser: true,
              useUnifiedTopology: true,
              useCreateIndex: true,
              useFindAndModify: false
            })
            .then(() => resolve(this.port))
            .catch((err: object) => reject(err))
        })
        .on('error', (err: object) => reject(err));
    })
  }
}

export default App;