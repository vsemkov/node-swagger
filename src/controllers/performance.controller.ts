import * as express from 'express';
import IPerformance from './../interfaces/performance.interface';
import IController from './../interfaces/controller.interface';
import PerformanceModel from './../models/performance.model'

class PerformanceController implements IController {
  public path = '/api/v1/performance';
  private router = express.Router();

  constructor() {
    this.intializeRoutes();
  }

  private intializeRoutes() {
    this.router.get(`${this.path}`, this.findAllRecords);
    this.router.get(`${this.path}/:id`, this.findRecordById);
    this.router.post(`${this.path}`, this.createRecord);
    this.router.put(`${this.path}/:id`, this.updateRecord);
    this.router.delete(`${this.path}/:id`, this.deleteRecord);
  }

  findAllRecords = (request: express.Request, response: express.Response) => {
    PerformanceModel.find().then(data => {
      if(data) {
        response.send(data)
      } else {
        response.sendStatus(404)
      }
    }).catch(e => {
      console.log(e)
      response.sendStatus(500)
    })
  }

  findRecordById = (request: express.Request, response: express.Response) => {
    PerformanceModel.findById(request.params.id).then(data => {
      if(data) {
        response.send(data)
      } else {
        response.sendStatus(404)
      }
    }).catch(e => {
      console.log(e)
      response.sendStatus(500)
    })
  }

  createRecord = (request: express.Request, response: express.Response) => {
    const performance: IPerformance = request.body;
    const newPerformance = new PerformanceModel(performance)

    newPerformance.save().then((data) => {
      response.send(data)
    }).catch(e => {
      console.log(e)
      response.sendStatus(500)
    })
  }

  deleteRecord = (request: express.Request, response: express.Response) => {
    PerformanceModel.findByIdAndDelete(request.params.id).then(data => {
      if(data) {
        response.sendStatus(204)
      } else {
        response.sendStatus(404)
      }
    }).catch(e => {
      console.log(e)
      response.sendStatus(500)
    })
  }

  updateRecord = (request: express.Request, response: express.Response) => {
    const performance: IPerformance = request.body;

    PerformanceModel.findByIdAndUpdate(request.params.id, performance).then(data => {
        if(data) {
          response.send(Object.assign(data, performance))
        } else {
          response.sendStatus(404)
        }
    }).catch(e => {
      console.log(e)
      response.sendStatus(500)
    })
  }

  getRouter() {
    return this.router
  }
}

export default PerformanceController;