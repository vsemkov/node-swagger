import * as express from 'express'
import IStudent from './../interfaces/student.interface'
import IController from './../interfaces/controller.interface'
import StudentModel from './../models/student.model'

class StudentsController implements IController {
  public path = '/api/v1/students';
  private router = express.Router();

  constructor() {
    this.intializeRoutes();
  }

  private intializeRoutes() {
    this.router.get(`${this.path}`, this.findAllRecords);
    this.router.get(`${this.path}/:id`, this.findRecordById);
    this.router.post(`${this.path}`, this.createRecord);
    this.router.put(`${this.path}/:id`, this.updateRecord);
    this.router.delete(`${this.path}/:id`, this.deleteRecord);
  }

  findAllRecords = (request: express.Request, response: express.Response) => {
    StudentModel.find().populate('performance').then(data => {
      if(data) {
        response.send(data)
      } else {
        response.sendStatus(404)
      }
    }).catch(e => {
      console.log(e)
      response.sendStatus(500)
    })
  }

  findRecordById = (request: express.Request, response: express.Response) => {
    StudentModel.findById(request.params.id).populate('performance').then(data => {
      if(data) {
        response.send(data)
      } else {
        response.sendStatus(404)
      }
    }).catch(e => {
      console.log(e)
      response.sendStatus(500)
    })
  }

  createRecord = (request: express.Request, response: express.Response) => {
    const student:IStudent = request.body
    const newStudent = new StudentModel(student)

    newStudent.save().then((saveData) => {
      StudentModel.findById(saveData._id).populate('performance').then(data => {
        response.send(data)
      })
    }).catch(e => {
      console.log(e)
      response.sendStatus(500)
    })
  }

  deleteRecord = (request: express.Request, response: express.Response) => {
    StudentModel.findByIdAndDelete(request.params.id).then((data) => {
      if(data) {
        response.sendStatus(204)
      } else {
        response.sendStatus(404)
      }
    }).catch(e => {
      console.log(e)
      response.sendStatus(500)
    })
  }

  updateRecord = (request: express.Request, response: express.Response) => {
    const student: IStudent = request.body
    StudentModel.findByIdAndUpdate(request.params.id, student).then(updateData => {
      if(updateData) {
        StudentModel.findById(updateData._id).populate('performance').then(data => {
          response.send(data)
        })
      } else {
        response.sendStatus(404)
      }
    }).catch(e => {
      console.log(e)
      response.sendStatus(500)
    })
  }

  getRouter() {
    return this.router
  }
}

export default StudentsController;