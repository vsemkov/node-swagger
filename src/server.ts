import app from './app'
import StudentsController from './controllers/students.controller';
import PerformanceController from './controllers/performance.controller';

const port:number = (process.env.PORT || 3000) as number

const server = new app([
  new StudentsController(),
  new PerformanceController()
], port).start()
  .then(port => console.log(`Server running on port ${port}`))
  .catch(error => {
    console.log(error)
    process.exit(1);
  });

export default server;