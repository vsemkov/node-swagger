import students from './students'
import performances from './performances'
import ui from './ui'

export default {
  students,
  performances,
  ui
}
