import { LOAD_PERFORMANCES } from '../constants/ActionTypes'

const initialState = {
  list: []
}

export default function performances(state = initialState, action) {
  switch (action.type) {
    case LOAD_PERFORMANCES: {
      return {
        ...state,
        list: action.performances
      }
    }
    default:
      return state
  }
}
