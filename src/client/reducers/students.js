import { LOAD_STUDENTS, UPDATE_STUDENT, REMOVE_STUDENT, ADD_STUDENT } from '../constants/ActionTypes'

const initialState = {
  list: []
}

export default function students(state = initialState, action) {
  switch (action.type) {
    case LOAD_STUDENTS: {
      return {
        ...state,
        list: action.students
      }
    }
    case UPDATE_STUDENT: {
      return {
        ...state,
        list: state.list.map(e => {
          return e._id !== action.student._id ? e : action.student
        })
      }
    }
    case REMOVE_STUDENT: {
      return {
        ...state,
        list: state.list.filter(e => e._id !== action.id)
      }
    }
    case ADD_STUDENT: {
      return {
        ...state,
        list: state.list.concat([action.student])
      }
    }
    default:
      return state
  }
}
