import { ERROR, LOADING, LOADED, CLEAR_ERROR, UPDATING, UPDATED } from '../constants/ActionTypes'

const initialState = {
  isError: false,
  isLoading: false,
  isUpdating: false,
  errorMessage: ''
}

export default function ui(state = initialState, action) {
  switch (action.type) {
    case CLEAR_ERROR:
      return {
        ...state,
        isError: false,
        isLoading: false,
        errorMessage: ''
      }
    case ERROR:
      return {
        ...state,
        isError: true,
        isLoading: false,
        errorMessage: action.message
      }
    case LOADING:
      return {
        ...state,
        isLoading: true
      }
    case LOADED:
      return {
        ...state,
        isLoading: false
      }
    case UPDATING:
      return {
        ...state,
        isUpdating: true
      }
    case UPDATED:
      return {
        ...state,
        isUpdating: false
      }
    default:
      return state
  }
}
