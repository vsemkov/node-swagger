import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as StudentsActions from '../action/students'
import * as UIActions from '../action/ui'
import StudentList from '../components/studentlist/student_list'

const mapStateToProps = state => {
  return {
    students: state.students.list,
    performances: state.performances.list,
    ui: state.ui
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      ...StudentsActions,
      ...UIActions
    },
    dispatch
  )
})

export default connect(mapStateToProps, mapDispatchToProps)(StudentList)
