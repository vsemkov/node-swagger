import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import App from '../components/app'
import * as PerformanceActions from '../action/performance'

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(PerformanceActions, dispatch)
})

export default connect(null, mapDispatchToProps)(App)
