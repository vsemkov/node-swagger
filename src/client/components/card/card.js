import React from 'react'
import Button from '../button/button'
import './card.css'

const Input = ({ headerText, onClose, children, isHideClose }) => {
  return (
    <div className="card">
      <div className="card__container">
        <div className="card__header">
          <span>{headerText}</span>
          {!isHideClose && (
            <Button onClick={onClose} className="card_close_button">
              <svg viewport="0 0 12 12" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <line x1="1" y1="11" x2="11" y2="1" stroke="black" strokeWidth="2" />
                <line x1="1" y1="1" x2="11" y2="11" stroke="black" strokeWidth="2" />
              </svg>
            </Button>
          )}
        </div>
        <div className="card__body">{children}</div>
      </div>
    </div>
  )
}

export default Input
