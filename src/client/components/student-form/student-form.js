import React, { useState } from 'react'
import './student-form.css'
import Input from '../input/input'
import Button from '../button/button'

const StudentForm = props => {
  const { student, performances, onSave, onCancel } = props
  const [editStudent, setEditStudent] = useState(
    student || {
      firstName: '',
      surrName: '',
      lastName: '',
      dateOfBirth: ''
    }
  )

  const handleChangeInput = name => event => {
    editStudent[name] = event.currentTarget.value
    setEditStudent(Object.assign({}, editStudent))
  }

  const handleChangePerformance = event => {
    if (event.currentTarget && event.currentTarget.value) {
      editStudent.performance = performances.find(e => e._id === event.currentTarget.value)
    }
  }

  const handleCancel = () => {
    if (typeof onCancel === 'function') onCancel()
  }

  const handleSave = () => {
    if (typeof onSave === 'function') onSave(editStudent)
  }

  return (
    <div className="studentform__body_controls">
      <div>
        <Input placeholder="First Name" value={editStudent.firstName} onChange={handleChangeInput('firstName')} />
      </div>
      <div>
        <Input placeholder="Surr Name" value={editStudent.surrName} onChange={handleChangeInput('surrName')} />
      </div>
      <div>
        <Input placeholder="Last Name" value={editStudent.lastName} onChange={handleChangeInput('lastName')} />
      </div>
      <div>
        <Input placeholder="Birth Day" value={editStudent.dateOfBirth} onChange={handleChangeInput('dateOfBirth')} />
      </div>
      <div>
        <select
          onBlur={handleChangePerformance}
          defaultValue={editStudent.performance ? editStudent.performance._id : null}
        >
          <option>Select performance</option>
          {(performances || []).map(performance => (
            <option key={performance._id} value={performance._id}>
              {performance.name}
            </option>
          ))}
        </select>
      </div>
      <div className="buttons">
        <Button className="btn_blue" onClick={handleSave}>
          Save
        </Button>
        <Button className="btn_red" onClick={handleCancel}>
          Cancel
        </Button>
      </div>
    </div>
  )
}

export default StudentForm
