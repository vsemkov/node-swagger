import React, { useEffect, useState } from 'react'
import Card from '../card/card'
import Button from '../button/button'
import Error from '../error/error'
import StudentFrom from '../student-form/student-form'

import './student_list.css'

const StudentList = ({ students, performances, ui, actions }) => {
  const [editStudent, setEditStudent] = useState(null)
  const [isNewStudent, setIsNewStudent] = useState(false)

  useEffect(() => {
    actions.getAllStudents()
  }, [])

  const handleRemove = id => () => {
    actions.removeStudent(id)
    setEditStudent(false)
  }

  const handleEdit = id => () => {
    setEditStudent(id)
  }

  const handleNew = () => {
    setIsNewStudent(true)
  }

  const handleСancel = () => {
    setEditStudent(null)
  }

  const handleСancelNew = () => {
    setIsNewStudent(false)
  }

  const handleSave = student => {
    actions.updateStudent(student)
    setEditStudent(null)
  }

  const handleSaveNew = student => {
    actions.addStudent(student)
    setIsNewStudent(false)
  }

  return (
    <div className="studentlist">
      {ui.isError && <Error onClose={() => actions.clearError()}>{ui.errorMessage}</Error>}
      <div className="studentlist__cards">
        {(students || []).map(student => (
          <Card
            key={student._id}
            headerText={`${student.firstName} ${student.surrName} ${student.lastName}`}
            onClose={handleRemove(student._id)}
          >
            {editStudent === student._id ? (
              <StudentFrom student={student} performances={performances} onSave={handleSave} onCancel={handleСancel} />
            ) : (
              <React.Fragment>
                <ul>
                  {student.performance && <li>Performance: {student.performance.name}</li>}
                  <li>Birthday: {student.dateOfBirth}</li>
                </ul>
                <div className="card__body_buttons">
                  <Button className="btn_blue" onClick={handleEdit(student._id)}>
                    Edit
                  </Button>
                </div>
              </React.Fragment>
            )}
          </Card>
        ))}
        <Card isHideClose headerText="Add a new student">
          {isNewStudent && (
            <StudentFrom performances={performances} onSave={handleSaveNew} onCancel={handleСancelNew} />
          )}
          <div className="card__body_buttons">
            {!isNewStudent && (
              <Button className="btn_blue" onClick={handleNew}>
                Create a new student
              </Button>
            )}
          </div>
        </Card>
      </div>
    </div>
  )
}

export default StudentList
