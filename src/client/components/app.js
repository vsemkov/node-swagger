import React, { useEffect } from 'react'
import Students from '../containers/students'

const App = ({ actions }) => {
  useEffect(() => {
    actions.getAllPreformance()
  }, [])

  return <Students />
}

export default App
