import * as types from '../constants/ActionTypes'

export const clearError = () => ({ type: types.CLEAR_ERROR })
