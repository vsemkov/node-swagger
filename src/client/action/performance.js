import * as types from '../constants/ActionTypes'
import config from '../config'

export const getAllPreformance = () => {
  return dispatch => {
    dispatch({ type: types.LOADING })
    fetch(`${config.apiUrl}/performance`)
      .then(response => response.json())
      .then(data => {
        dispatch({ type: types.LOADED })
        dispatch({ type: types.LOAD_PERFORMANCES, performances: data })
      })
      .catch(() => {
        dispatch({ type: types.ERROR, message: 'Fetch data error' })
      })
  }
}
