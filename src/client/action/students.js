import * as types from '../constants/ActionTypes'
import config from '../config'

export const getAllStudents = () => {
  return dispatch => {
    dispatch({ type: types.LOADING })
    fetch(`${config.apiUrl}/students`)
      .then(response => response.json())
      .then(data => {
        dispatch({ type: types.LOADED })
        dispatch({ type: types.LOAD_STUDENTS, students: data })
      })
      .catch(() => {
        dispatch({ type: types.ERROR, message: 'Fetch data error' })
      })
  }
}

export const addStudent = student => {
  return dispatch => {
    dispatch({ type: types.LOADING })
    fetch(`${config.apiUrl}/students`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(student)
    })
      .then(response => response.json())
      .then(data => {
        dispatch({ type: types.LOADED })
        dispatch({ type: types.ADD_STUDENT, student: data })
      })
      .catch(() => {
        dispatch({ type: types.ERROR, message: 'Fetch data error' })
      })
  }
}

export const updateStudent = student => {
  return dispatch => {
    dispatch({ type: types.UPDATING })
    fetch(`${config.apiUrl}/students/${student._id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(student)
    })
      .then(response => response.json())
      .then(data => {
        dispatch({ type: types.UPDATED })
        dispatch({ type: types.UPDATE_STUDENT, student: data })
      })
      .catch(() => {
        dispatch({ type: types.ERROR, message: 'Fetch data error' })
      })
  }
}

export const removeStudent = id => {
  return dispatch => {
    dispatch({ type: types.REMOVE_STUDENT, id })
    dispatch({ type: types.UPDATING })
    fetch(`${config.apiUrl}/students/${id}`, {
      method: 'DELETE'
    })
      .then(() => {
        dispatch({ type: types.UPDATED })
        dispatch({ type: types.REMOVE_STUDENT, id })
      })
      .catch(() => {
        dispatch({ type: types.ERROR, message: 'Fetch data error' })
      })
  }
}
