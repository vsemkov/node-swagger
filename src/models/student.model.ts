import mongoose, { Schema, Document } from 'mongoose'
import IStudent from './../interfaces/student.interface'

export interface IStudentModel extends IStudent, Document {}

const StudentSchema:Schema = new Schema({
    firstName: { type: String, required: false, unique: false },
    lastName: { type: String, required: false, unique: false },
    surrName: { type: String, required: false, unique: false },
    dateOfBirth: { type: String, required: false, unique: false },
    performance: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Performance'
    }
});

export default mongoose.model<IStudentModel>('Student', StudentSchema)