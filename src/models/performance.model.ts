import mongoose, { Schema, Document } from 'mongoose'
import IPerformance from './../interfaces/performance.interface'

export interface IPerformanceModel extends IPerformance, Document {}

const PerformanceSchema:Schema = new Schema({
    name: { type: String, required: false, unique: false },
    value: { type: Number, required: false, unique: false }
});

export default mongoose.model<IPerformanceModel>('Performance', PerformanceSchema)