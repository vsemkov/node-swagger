interface IPerformance {
    name: string
    value: number
  }

  export default IPerformance