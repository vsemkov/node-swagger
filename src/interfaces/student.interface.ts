interface IStudent {
    firstName: string
    lastName: string
    surrName: string
    dateOfBirth: string
    performance: object
  }

  export default IStudent